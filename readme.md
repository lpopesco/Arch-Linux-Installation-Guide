# Minimal guide to installing Arch Linux with FDE on UEFI

Foreward: This guide will guide you to set up a fully encrypted Arch system quickly and simply. Some options have been chosen per my own preferences, or for the sake of simplicity. For deeper documentation and understanding, refer to the [Arch Wiki's installation guide](https://wiki.archlinux.org/index.php/installation_guide).

## Setup

I will be using /dev/sdA for reference in this guide. Note the capital A, which should prevent careless copy-pasting from destroying your computer.

Download the Arch image from https://www.archlinux.org/download/ and verify its GPG signature.

Burn the image to a USB `dd if=archlinux-*.iso of=/dev/sdB bs=16M && sync`

## Hard Drive Preparation

### Partitioning

**Warning: This will wipe your drive**

Open the drive in gdisk: `gdisk /dev/sdA`

Start by creating a new empty GUID partition table (o)

You'll create the 3 following partitions (n):

Partition 1: 100 MB EFI partition (Hex code EF00)  
Partition 2: 250 MB boot partition (hex code 8300)  
Partition 3: The rest of the space for root & swap (hex code 8300)

Review the changes (p) & write them with (w).

### Setup File Systems

Setup the boot and EFI partitions

    mkfs.vfat -F 32 /dev/sdA1
    mkfs.ext2 /dev/sdA2 # /boot can also be ext4 or btrfs, it's up to you.

#### Setup encryption & LVM

Encrypt your root partition

`cryptsetup -c aes-xts-plain64 -h sha512 -s 512 --use-random luksFormat /dev/sdA3`

Open your encrypted partition

`cryptsetup luksOpen /dev/sdA3 ArchFS  # The name is entirely arbitrary, use whatever you like. Just be consistent.`

Next, create the LVM partitions. If you're using BTRFS as this guide suggests, do not use a swap file!

Modify these commands as needed for size:

    pvcreate /dev/mapper/ArchFS
    vgcreate Arch /dev/mapper/ArchFS
    lvcreate -L +2G Arch -n swap
    lvcreate -l +100%FREE Arch -n root

Next, create the file systems on the LVM partitions:

    mkswap /dev/mapper/Arch-swap
    mkfs.btrfs /dev/mapper/Arch-root

## Installing Arch

Mount the new system:

    mount /dev/mapper/Arch-root /mnt
    swapon /dev/mapper/Arch-swap
    mkdir /mnt/boot
    mount /dev/sdA2 /mnt/boot
    mkdir /mnt/boot/efi
    mount /dev/sdA1 /mnt/boot/efi

Edit the mirror list to optimize download speeds 

`vim /etc/pacman.d/mirrorlist`

Install Arch Linux:

`pacstrap /mnt base base-devel grub-efi-x86_64 efibootmgr dialog btrfs-progs wpa_supplicant`

Automatically generate fstab, then review it to make sure it's correct

`genfstab -U /mnt >> /mnt/etc/fstab`

chroot into the new system to begin setting it up

`arch-chroot /mnt /bin/bash`

Assign your hostname:

`echo ArchBox > /etc/hostname # Use any hostname you want`

Set your locale (assuming you're English)

1) Edit `/etc/locale.gen` and uncomment `en_US.UTF UTF-8`  
2) Edit `/etc/locale.conf` and add **only**: `LANG=en_US.UTF-8`

Generate the locale:

`locale-gen`

Set the root password:

`passwd`

Add your user and set your password:

    useradd -m -G wheel -s /bin/bash ArchUser
    passwd ArchUser

Enable wheel group for sudo

`visudo`  
Uncomment the following line:  
`%wheel ALL=(ALL) ALL`

Configure mkinitcpio with the correct hooks and modules:

`vi /etc/mkinitcpio.conf`

For MODULES, add btrfs, so it looks like this:

`MODULES=(btrfs)`

Your HOOKS should look like this:

`HOOKS=(base udev autodetect modconf block keymap encrypt lvm2 resume filesystems keyboard fsck)`

Generate the image:

`mkinitcpio -p linux`

Next, install grub:

`grub-install --efi-directory=/boot/efi --target=x86_64-efi --bootloader-id=ArchLinux`

Edit `/etc/default/grub`:

Modify `GRUB_CMDLINE_LINUX` so it looks like this:    
`GRUB_CMDLINE_LINUX="cryptdevice=/dev/sdA3:ArchFS resume=/dev/mapper/Arch-swap"`    
Remember to modify 'ArchFS' to match the value you gave earlier. If you aren't using swap, remove the resume statement.

Next, uncomment this line:    
`GRUB_ENABLE_CRYPTODISK=y`

Generate your grub config:

`grub-mkconfig -o /boot/grub/grub.cfg`

Now exit your new Arch installation, and unmount all partitions:

`exit`  

    umount -R /mnt
    swapoff -a

Reboot your machine, and have fun configuring your new install!

`reboot`

## Conclusion

That concludes this guide for installing an encrypted Arch Linux on BTRFS. If there are any problems, updates that I miss, or questions, open them in an issue and I'll get back to you.